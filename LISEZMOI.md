ATTENTION 

Ceci est un projet netbeans, les fichiers JAVA contenant le code se situe sous
 sudoku/src/mvc

Grille test disponibles, pour les tester charger la partie dans l'application : grille1.txt ou grille2.txt

Lors de la création d'une nouvelle partie, si l'application n'a pas les droits ROOT (sous linux), il se peut que la création de partie échoue car il est impossible d'enregistrer la partie. Dans ce cas là, veuillez modifier les chmod dans le dossier du programme ( chmod 774 pour éviter des groupes d'exec les fichiers du dossier).
