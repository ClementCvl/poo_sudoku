/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc;

import java.io.*;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Random;

/**
 *
 * @author p1406664
 */
public class Modele_jeu extends Observable {

    public static int nb_ligne = 9;
    public static int nb_col = 9;
    public static int nb_sous_carre = 3; //sous carré par ligne ou col

    //tableau de ligne, colonne, sous carré
    private ArrayList<Groupe> tab_c;
    private ArrayList<Groupe> tab_l;
    private ArrayList<ArrayList<Groupe>> tab_ca;

    //actual score (pas 0 si une partie sauv) et le best
    private int actual_score = 0;
    private int best_score = 0;

    //var pour timer, on le met dans model
    private Temps temps;

    public Modele_jeu() {
        createVariable();

    }

    public void init(String name) {
        String data = readFile(name);
        String[] tab_data;
        //on essaie de lire le score + temps
        try {
            String[] temp_data = data.split("_");
            data = temp_data[0];
            String data_ = temp_data[1];
            String[] temp_data_2 = data_.split("/");
            String data_score = temp_data_2[0];
            String data_temps = temp_data_2[1];
            initScore(data_score);
            initTemps(data_temps);
            tab_data = data.split(" ");
            //si problemen, on met temps et score a 0 et on lit comme si de riuen n'était
        } catch (java.lang.ArrayIndexOutOfBoundsException e) {
            tab_data = data.split(" ");
            initScore("0 0");
            initTemps("0 0");
        }

        //on initialise les tableaux pour pouvoir joué grace a notre tab remplit plus haut
        int var = 0;
        for (int i = 0; i < nb_ligne; i++) {
            for (int j = 0; j < nb_col; j++) {
                Case c;
                if (tab_data[var].equals("0") || tab_data[var].equals("0n")) {
                    c = new CaseNonBloquee(Valeur.ZERO);
                } else if (tab_data[var].equals("1n") || tab_data[var].equals("2n") || tab_data[var].equals("3n") || tab_data[var].equals("4n") || tab_data[var].equals("5n") || tab_data[var].equals("6n") || tab_data[var].equals("7n") || tab_data[var].equals("8n") || tab_data[var].equals("9n")) {
                    String[] data2 = tab_data[var].split("n");
                    c = new CaseNonBloquee(Valeur.getVal(data2[0]));
                } else {
                    c = new CaseBloquee(Valeur.getVal(tab_data[var]));
                }
                var++;
                tab_l.get(i).addCase(c);
                tab_c.get(j).addCase(c);
                tab_ca.get(i / 3).get(j / 3).addCase(c);

                c.getListGroupe().add(tab_l.get(i));
                c.getListGroupe().add(tab_c.get(j));
                c.getListGroupe().add(tab_ca.get(i / nb_sous_carre).get(j / nb_sous_carre));
            }
        }
    }

    //attribue une valeur a une case ligne x col y
    public boolean setValue(int x, int y, String value) {
        if (tab_l.get(x).getList().get(y).getClass() == CaseNonBloquee.class) {
            ((CaseNonBloquee) tab_l.get(x).getList().get(y)).update(Valeur.getVal(value));
            return true;
        } else {
            return false;
        }
    }

    //verifie la mise en place de la value, et notif lobservers
    public boolean verifieApresEntree(int ligne, int col, String chiffre) {
        boolean b = setValue(ligne, col, chiffre);
        if (chiffre != " ") {
            actual_score++;
        }
        setChanged();
        notifyObservers();
        return b;
    }

//nous sert dans VERIFPARTIETERMINEE, si check renvoie 0, il y a donc 0 conflits
/*
        Cete fct cherche si dans les lignes/col/carré du sudoku, il y a des
        chiffres identiques, si oui, la fct
        (true).
        On retourne la variable conflict, qui signifie combien de casé colorié (fois deux)
        il y a eu dans la fonction.
     */
    public int check() {

        ArrayList<Valeur> check_list = new ArrayList<>();
        int conflict = 0;
        //on a pour idée de parcourir ligne, colonne et sous carré pour voir si sudo bon
        for (int i = 0; i < nb_ligne; i++) {
            for (Groupe g : tab_l) {
                for (Case c : g.getList()) {
                    if (c.getV() != Valeur.ZERO) {
                        //on re initialise toute les cases conflit a false, on efface le tableau
                        //si la valeur de la case deja dans checklist, rpesente deux fois, pas bon !
                        if (!check_list.contains(c.getV())) {
                            check_list.add(c.getV());
                        } //presente deux fois
                        else if (check_list.contains(c.getV())) {
                            conflict++;
                        }
                    }
                }
                check_list = new ArrayList<>();
            }
            check_list = new ArrayList<>();
        }

        //TEST pour col
        check_list = new ArrayList<>();
        for (int i = 0; i < nb_col; i++) {
            for (Groupe g : tab_c) {
                for (Case c : g.getList()) {
                    if (c.getV() != Valeur.ZERO) {
                        if (!check_list.contains(c.getV())) {
                            check_list.add(c.getV());
                        } else if (check_list.contains(c.getV())) {
                            conflict++;
                        }
                    }
                }
                check_list = new ArrayList<>();
            }
            check_list = new ArrayList<>();
        }

        //Test NB sous carré
        check_list = new ArrayList<>();
        for (int i = 0; i < nb_sous_carre; i++) {
            for (ArrayList<Groupe> list_g : tab_ca) {
                for (Groupe g : list_g) {
                    for (Case c : g.getList()) {
                        if (c.getV() != Valeur.ZERO) {
                            if (!check_list.contains(c.getV())) {
                                check_list.add(c.getV());
                            } else if (check_list.contains(c.getV())) {
                                conflict++;
                            }
                        }
                    }
                    check_list = new ArrayList<>();
                }
                check_list = new ArrayList<>();
            }
        }

        return conflict;
    }

    //creer un sudoku, utile pour nouvelle partie
    public void createSudoku(int nb, String name) throws IOException {
        init("000.txt");
        resolveRandom(0);
        enleverCase(nb);
        sauvegarderPartieNewSudo(name);
    }

    //enleve nb cases au hsard dans le sudoku, pour pouvoir etre joué
    public void enleverCase(int nb) {
        int var = 0, i = 0, j = 0;
        while (var != nb) {
            if (tab_l.get(i).getList().get(j).getV().ordinal() != 0) {
                setValue(i, j, "0");
                var++;
            }
            i = getRand(9, 0);
            j = getRand(9, 0);
        }

    }

    //resolve de maniere random, cela permet de "resoudre" un sudoku composé d'aucun chiffre
    //random est bien pour créer, resolve est bien pour resoudre tout court
    public boolean resolveRandom(int position) {
        CaseNonBloquee c = null;
        if (position == 81) {
            return true;
        }
        int i = position / 9, j = position % 9;

        try {
            if (tab_l.get(i).getList().get(j).getV().ordinal() != 0) {
                return resolveRandom(position + 1);
            }

        } catch (java.lang.IndexOutOfBoundsException e) {
            return resolveRandom(position + 1);
        }
        ArrayList<Integer> tab = new ArrayList();
        int nb = 9;
        while (tab.size() < 8) {
            nb = getRand(9, 1);
            if (!tab.contains(nb)) {
                c = valueCase(nb);
                tab.add(nb);
                if (!tab_l.get(i).estEnConflit(c) && !tab_c.get(j).estEnConflit(c) && !tab_ca.get(i / 3).get(j / 3).estEnConflit(c)) {
                    ajoutList(i, j, c);
                    if(resolveRandom(position + 1))
                        return true;
                }
            }
        }
        c = new CaseNonBloquee(Valeur.ZERO);

        ajoutList(i, j, c);
        return false;
    }

    //resolve un sudoku a partir d'une position x
    public boolean resolve(int position) {
        CaseNonBloquee c = null;
        if (position == 81) {
            return true;
        }
        int i = position / 9, j = position % 9;
        try {
            if (tab_l.get(i).getList().get(j).getV().ordinal() != 0) {
                return resolve(position + 1);
            }

        } catch (java.lang.IndexOutOfBoundsException e) {
            return resolve(position + 1);
        }

        for (int k = 1; k <= 9; k++) {
            c = valueCase(k);
            if (!tab_l.get(i).estEnConflit(c) && !tab_c.get(j).estEnConflit(c) && !tab_ca.get(i / 3).get(j / 3).estEnConflit(c)) {
                ajoutList(i, j, c);
                if (resolve(position + 1)) {
                    return true;
                }
            }
        }
        c = new CaseNonBloquee(Valeur.ZERO);

        ajoutList(i, j, c);
        return false;
    }

    //ajoute une case au position lgine i et col j dans les listes et ses listes
    public void ajoutList(int i, int j, Case c) {
        tab_l.get(i).addCase(j, c);
        tab_c.get(j).addCase(i, c);
        if (i % 3 == 0) {
            tab_ca.get(i / 3).get(j / 3).addCase(j % 3, c);
        } else if (i % 3 == 1) {
            tab_ca.get(i / 3).get(j / 3).addCase((j % 3) + 3, c);
        } else if (i % 3 == 2) {
            tab_ca.get(i / 3).get(j / 3).addCase((j % 3) + 6, c);
        }

        c.getListGroupe().add(tab_l.get(i));
        c.getListGroupe().add(tab_c.get(j));
        c.getListGroupe().add(tab_ca.get(i / 3).get(j / 3));
    }

    //créer une case avec nb alea en traduisant en valeur
    public CaseNonBloquee valueCase(int nb_alea) {
        CaseNonBloquee c = null;
        switch (nb_alea) {
            case 1:
                c = new CaseNonBloquee(Valeur.ONE);
                break;
            case 2:
                c = new CaseNonBloquee(Valeur.TWO);
                break;
            case 3:
                c = new CaseNonBloquee(Valeur.THREE);
                break;
            case 4:
                c = new CaseNonBloquee(Valeur.FOUR);
                break;
            case 5:
                c = new CaseNonBloquee(Valeur.FIVE);
                break;
            case 6:
                c = new CaseNonBloquee(Valeur.SIX);
                break;
            case 7:
                c = new CaseNonBloquee(Valeur.SEVEN);
                break;
            case 8:
                c = new CaseNonBloquee(Valeur.HEIGHT);
                break;
            case 9:
                c = new CaseNonBloquee(Valeur.NINE);
                break;
        }

        return c;
    }

    //regarde si il reste des zero pour verifier si une partie est bienf ifini
    public boolean lookingFor0() {
        for (int i = 0; i < nb_ligne; i++) {
            for (Groupe g : tab_l) {
                for (Case c : g.getList()) {
                    if (c.getV() == Valeur.ZERO) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    public boolean verifPartieTermine() {
        if (check() == 0 && lookingFor0()) {
            return true;
        }
        return false;
    }

    @Override
    public void notifyObservers() {
        super.notifyObservers(); //To change body of generated methods, choose Tools | Templates.
    }

    //verifie si une case est repassé blanche ou aps, suite a un changement de joueur
    // utilise verif conf pour cela
    public void verif_white() {
        for (Groupe g : getTabL()) {
            for (Case c : g.getList()) {
                Valeur v = c.getV();
                if (v != Valeur.ZERO && c instanceof CaseNonBloquee) {
                    c.setConf(verifConf(c));
                }
                else if (v == Valeur.ZERO)
                        c.setConf(false);
            }
        }
    }

    public boolean verifConf(Case c) {
        boolean conflit = false;
        for (Groupe g : c.getListGroupe()) {
            if (g.estEnConflit(c)) {
                conflit = true;
                break;
            }
        }
        return conflit;
    }

    public void chargerPartie(String name) {
        init(name);
    }

    public void sauvegarderPartie(String name) throws IOException {
        FileWriter fichier = new FileWriter(name);
        int cpt = 0;
        for (Groupe p : tab_l) {
            for (Case c : p.getList()) {
                if (c instanceof CaseNonBloquee) {
                    fichier.write(Integer.toString(c.getV().ordinal()) + "n" + " ");
                } else {
                    fichier.write(Integer.toString(c.getV().ordinal()) + " ");
                }

                cpt++;
                if (cpt == 9) {
                    fichier.write("\n");
                    cpt = 0;
                }
            }
        }
        if (actual_score > best_score) {
            fichier.write("_" + actual_score + " " + actual_score);
        } else {
            fichier.write("_" + actual_score + " " + best_score);
        }
        fichier.write("/" + temps.getMinute() + " " + temps.seconde);
        fichier.close();
    }

    //idem que sauvegarde, utile car sinon cela rajoute les n car non bloqué de base pour l'algo de remplissage
        public void sauvegarderPartieNewSudo(String name) throws IOException {
        FileWriter fichier = new FileWriter(name);
        int cpt = 0;
        for (Groupe p : tab_l) {
            for (Case c : p.getList()) {
                fichier.write(Integer.toString(c.getV().ordinal()) + " ");
                if (cpt == 9) {
                    fichier.write("\n");
                    cpt = 0;
                }
            }
        }
        if (actual_score > best_score) {
            fichier.write("_" + actual_score + " " + actual_score);
        } else {
            fichier.write("_" + actual_score + " " + best_score);
        }
        fichier.write("/" + temps.getMinute() + " " + temps.seconde);
        fichier.close();
    }
        //read file...
    public String readFile(String name) {
        String data = "";
        try {
            InputStream ips = new FileInputStream(name);
            InputStreamReader ipsr = new InputStreamReader(ips);
            BufferedReader br = new BufferedReader(ipsr);
            String ligne = "";
            ligne = br.readLine();
            while (ligne != null) {
                data += ligne;
                ligne = br.readLine();
            }
            br.close();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return data;
    }

    protected void initScore(String data) {
        String[] score_tab = data.split(" ");
        actual_score = Integer.decode(score_tab[0]);
        best_score = Integer.decode(score_tab[1]);
    }

    protected void initTemps(String data) {
        String[] tab = data.split(" ");
        temps.setMinute(Integer.parseInt(tab[0]));
        temps.setSeconde(Integer.parseInt(tab[1]));
    }

    public void createVariable() {
        tab_c = new ArrayList<Groupe>();
        tab_l = new ArrayList<Groupe>();
        tab_ca = new ArrayList<ArrayList<Groupe>>();

        for (int i = 0; i < 9; i++) {
            tab_c.add(new Groupe());
            tab_l.add(new Groupe());
            for (int j = 0; i < 3 && j < 3; j++) { // ajout de trois sous carré
                tab_ca.add(new ArrayList<Groupe>());
                for (int k = 0; k < 3; k++) {//ajout de trois sous carré pour chaque ligne/col
                    tab_ca.get(i).add(new Groupe());
                }
            }
        }

        temps = new Temps();
    }

    public int getRand(int max, int min) {
        Random r = new Random();
        int nb = r.nextInt(max) + min;
        return nb;
    }

    public void initSudoku() {
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (j > 3) {
                    tab_l.get(i).addCase(new CaseNonBloquee(Valeur.ZERO));
                } else {
                    tab_l.get(i).addCase(new CaseNonBloquee(Valeur.ZERO));
                }
            }
        }
    }

    public void display() {
        int nb_ligne = 0;
        for (Groupe g : tab_l) {
            for (int j = 0; j < 9; j++) {
                if (j % 3 == 0) {
                    System.out.print("|");
                }
                int sd = g.getList().get(j).getV().ordinal();
                String s = Integer.toString(sd);
                if (g.getList().get(j).getConf()) {
                    System.out.print(sd + "(c) ");
                } else {
                    System.out.print(sd + " ");
                }
            }
            nb_ligne++;
            System.out.print("|");
            System.out.println("");
            if (nb_ligne == 3) {
                System.out.println("---------------------------------------");
                nb_ligne = 0;
            }
        }
        System.out.println("__________END____________");
    }

    public void setTabC(ArrayList<Groupe> tab_c) {
        this.tab_c = tab_c;
    }

    public void setTabL(ArrayList<Groupe> tab_l) {
        this.tab_l = tab_l;
    }

    public void setTabCa(ArrayList<ArrayList<Groupe>> tab_ca) {
        this.tab_ca = tab_ca;
    }

    public ArrayList<Groupe> getTabC() {
        return tab_c;
    }

    public ArrayList<Groupe> getTabL() {
        return tab_l;
    }

    public ArrayList<ArrayList<Groupe>> getTabCa() {
        return tab_ca;
    }

    public int getActualScore() {
        return actual_score;
    }

    public int getBestScore() {
        return best_score;
    }

    public void setActualScore(int var) {
        this.actual_score = var;
    }

    public Temps getTemps() {
        return temps;
    }

    public StringBuilder splitReadme(String str){
        int index = 0;
        int NB_MAX = 1;
        StringBuilder chaineFinal = new StringBuilder(str.length());
        while(index < str.length()) {
          int pointIndex = str.indexOf('.', index + NB_MAX);

          if(pointIndex < index) { // inclut < 0, donc espace non trouvé : on a fini
            chaineFinal.append(str.substring(index));
            index = str.length();
          }

          else {
            chaineFinal.append(str.substring(index, pointIndex));
            chaineFinal.append('\n');
            index = pointIndex + 1;
          }
        }


        return chaineFinal;
    }

}
