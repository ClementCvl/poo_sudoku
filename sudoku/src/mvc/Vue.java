/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc;

import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.*;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Pair;

/**
 *
 * @author p1406664
 */
public class Vue extends Application {

    //observer ca update (role du conroler)
    Stage stage;
    Scene scene;
    Modele_jeu jeu;
    GridPane gPane;
    MenuBar menu_bar;
    Menu fichier_menu;
    MenuItem partie_item;
    MenuItem charger_item;
    MenuItem sauvegarder_item;
    MenuItem quitter_item;
    Menu aide_menu;
    MenuItem resolution_item;
    MenuItem aide_item;

    Text score;
    Text txt_temps;
    VBox txt_box;

    TextInputDialog dialog;
    TextField nb_case;
    TextField name_file;

    public Vue() {

    }

    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        jeu = new Modele_jeu();
        jeu.init("000.txt");

        // gestion du placement (permet de placer le champ Text affichage en haut, et GridPane gPane au centre)
        initGraph();

        initListenersSudoku();

        initStage();

    }

    public void initGraph() {
        BorderPane border = new BorderPane();
        Label label = new Label();

        menu_bar = new MenuBar();
        fichier_menu = new Menu("Fichier");
        aide_menu = new Menu("Aide");

        menu_bar.getMenus().setAll(fichier_menu, aide_menu);

        partie_item = new MenuItem("Nouvelle partie");
        partie_item.setAccelerator(new KeyCodeCombination(KeyCode.F2));
        charger_item = new MenuItem("Charger partie");
        charger_item.setAccelerator(new KeyCodeCombination(KeyCode.F3));
        sauvegarder_item = new MenuItem("Sauvegarder partie");
        sauvegarder_item.setAccelerator(new KeyCodeCombination(KeyCode.F4));
        quitter_item = new MenuItem("Quitter");
        quitter_item.setAccelerator(new KeyCodeCombination(KeyCode.F7));
        fichier_menu.getItems().setAll(partie_item, charger_item, sauvegarder_item, new SeparatorMenuItem(), quitter_item);

        resolution_item = new MenuItem("Résolution complète");
        resolution_item.setAccelerator(new KeyCodeCombination(KeyCode.F5));
        aide_item = new MenuItem("Explications");
        aide_item.setAccelerator(new KeyCodeCombination(KeyCode.F6));
        aide_menu.getItems().setAll(resolution_item, new SeparatorMenuItem(), aide_item);

        menu_bar.setUseSystemMenuBar(true);
        // permet de placer les differents boutons dans une grille
        gPane = new GridPane();
        gPane.setConstraints(label, 9, 9);
        gPane.resize(400, 400);

        jeu.addObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                jeu.verif_white();
                reload();
                if (jeu.verifPartieTermine()) {
                    partieFinie();
                }
                score.setText("Score : " + Integer.toString(jeu.getActualScore()) + " Best : " + Integer.toString(jeu.getBestScore()));
            }
        });

        jeu.getTemps().addObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                txt_temps.setText(" Temps : " + jeu.getTemps().getMinute() + ":" + jeu.getTemps().getSeconde());
            }
        });

        //jeu.addObserver((Observer) this);
        score = new Text("Score : " + Integer.toString(jeu.getActualScore()) + " Best : " + Integer.toString(jeu.getBestScore()));
        txt_temps = new Text(" Temps : " + jeu.getTemps().getMinute() + ":" + jeu.getTemps().getSeconde());

        txt_box = new VBox();
        txt_box.setSpacing(10);
        txt_box.setPadding(new Insets(10, 0, 0, 0));
        txt_box.getChildren().addAll(score, txt_temps);

        border.setCenter(gPane);
        border.setTop(menu_bar);
        border.setBottom(txt_box);
        scene = new Scene(border, Color.LIGHTBLUE);
        scene.getStylesheets().add("style.css");

        gPane.setGridLinesVisible(true);
    }

    public void initStage() {
        stage.setTitle("Sudoku");
        stage.setScene(scene);
        stage.show();
    }

    //se lance quand le modele nous a notif
    public void reload() {
        int i = 0;
        for (Groupe g : jeu.getTabC()) {
            for (Case c : g.getList()) {
                if (c.getConf() == true) {
                    for (Node n : gPane.getChildren()) {
                        if (n instanceof Button) {
                            if (n.getId().equals(Integer.toString(i))) {
                                Button b_c = (Button) n;
                                b_c.setStyle("-fx-base : #E9383F;");
                            }
                        }
                    }
                } else if (c.getConf() == false && c instanceof CaseNonBloquee) {
                    for (Node n : gPane.getChildren()) {
                        if (n instanceof Button) {
                            if (n.getId().equals(Integer.toString(i))) {
                                Button b_c = (Button) n;
                                b_c.setStyle("-fx-base : white;");
                            }
                        }
                    }
                }
                i++;
            }
        }
    }
    //petit bug de fin de aprite quand on fait new gfame... deux fois...

    public static void main(String[] args) throws IOException {
        launch(args);
        System.exit(0);
    }

    public Pair<String, String> creationDialogue() {
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Nouvelle Partie");
        dialog.setHeaderText("Paramètre pour créer la nouvelle partie");

        ButtonType validationButtonType = new ButtonType("Ok", ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(validationButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        nb_case = new TextField();
        nb_case.setPromptText("40");
        name_file = new TextField();
        name_file.setPromptText("enregistré à la racine");

        grid.add(new Label("Nombre de cases à enlever:"), 0, 0);
        grid.add(nb_case, 1, 0);
        grid.add(new Label("Nom du fichier:"), 0, 1);
        grid.add(name_file, 1, 1);

        Node validationButton = dialog.getDialogPane().lookupButton(validationButtonType);
        validationButton.setDisable(true);
        name_file.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!nb_case.getText().isEmpty()) {
                validationButton.setDisable(newValue.trim().isEmpty());
            }
        });

        nb_case.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!name_file.getText().isEmpty()) {
                validationButton.setDisable(newValue.trim().isEmpty());
            }
        });

        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == validationButtonType) {
                return new Pair<>(nb_case.getText(), name_file.getText());
            }
            return null;
        });

        Optional<Pair<String, String>> result = dialog.showAndWait();

        return result.get();
    }

    public void initListenersSudoku() {
        int i = 0;
        DropShadow shadow = new DropShadow();
        partie_item.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                creerPartie();
            }
        });

        charger_item.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                chargerPartie();
            }
        });

        sauvegarder_item.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                sauvegarderPartie();
            }
        });

        quitter_item.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                System.exit(0);
            }
        });

        resolution_item.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                jeu.resolve(0);
                initListenersSudoku();
                if (jeu.verifPartieTermine()) {
                    partieFinie();
                }
            }
        });

        aide_item.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                createTutorialDialogue();
            }
        });

        for (Groupe g : jeu.getTabC()) {
            for (Case c : g.getList()) {
                int sd = c.getV().ordinal();
                final Button b;
                if (sd == 0) {
                    b = new Button("  ");
                } else {
                    b = new Button(Integer.toString(sd));
                }
                b.setId(Integer.toString(i));
                b.setMaxHeight(40);
                b.setMaxWidth(40);
                b.setFont(Font.font("Verdana", 20));
                b.setTextAlignment(TextAlignment.CENTER);
                b.setOnMouseClicked(new EventHandler<MouseEvent>() {
                    public void handle(MouseEvent event) {
                        //On regarde si une touche est pressée
                        //Si cette touche est un chiffre
                        //Verification pour le placement
                        //Ecriture du chiffre
                        b.setOnKeyTyped(new EventHandler<KeyEvent>() {
                            public void handle(KeyEvent ke) {
                                String chiffre = ke.getCharacter();
                                if (chiffre.equals("1") || chiffre.equals("2") || chiffre.equals("3") || chiffre.equals("4") || chiffre.equals("5") || chiffre.equals("6") || chiffre.equals("7") || chiffre.equals("8") || chiffre.equals("9")) {
                                    if (jeu.verifieApresEntree(gPane.getRowIndex(b), gPane.getColumnIndex(b), chiffre)) {
                                        b.setText(chiffre);
                                    }
                                }
                            }
                        });
                        b.setOnKeyReleased(new EventHandler<KeyEvent>() {
                            public void handle(KeyEvent ke) {
                                KeyCode code = ke.getCode();
                                if (code == KeyCode.BACK_SPACE || code == KeyCode.DELETE) {
                                    String chiffre = "  ";
                                    if (jeu.verifieApresEntree(gPane.getRowIndex(b), gPane.getColumnIndex(b), chiffre)) {
                                        b.setText(chiffre);
                                    }
                                }
                            }
                        });

                    }
                });
                b.addEventHandler(MouseEvent.MOUSE_ENTERED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        b.setEffect(shadow);
                    }
                });

                b.addEventHandler(MouseEvent.MOUSE_EXITED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        b.setEffect(null);
                    }
                });
                if (c instanceof CaseBloquee) {
                    b.setStyle("-fx-base : #98FB98;-fx-background-insets: 0, 0 1 0 0 ;");
                }

                //css : -fx-border-width: 0 2px 0 0;-fx-border-color: #565656;-fx-border-style: solid;
                int col = jeu.getTabC().indexOf(g);
                int ligne = jeu.getTabC().get(jeu.getTabC().indexOf(g)).getList().indexOf(c);
                if (col == 2 || col == 5) {
                    if (ligne != 2 && ligne != 5) {
                        b.getStyleClass().add("borderLeft");
                    } else {
                        b.getStyleClass().add("borderLeftBottom");
                    }
                }
                if (ligne == 2 || ligne == 5) {
                    b.getStyleClass().add("borderBottom");
                }
                i++;
                gPane.add(b, col, ligne);
            }
        }
    }

    public void initObserver() {
        initGraph();
        jeu.addObserver(new Observer() {
            @Override
            public void update(Observable o, Object arg) {
                jeu.verif_white();
                reload();
                if (jeu.verifPartieTermine()) {
                    partieFinie();
                }
            }
        });

        initListenersSudoku();
        initStage();
    }

    public void reloadChiffre() {
        int i = 0;
        for (Node n : gPane.getChildren()) {
            if (n instanceof Button) {
                if (n.getId().equals(Integer.toString(i))) {
                    Case c = jeu.getTabL().get(i % 9).getList().get(i / 9);
                    Button b_c = (Button) n;
                    b_c.setText(Integer.toString(c.getV().ordinal()));
                    i++;
                }
            }
        }
    }

    public void partieFinie() {
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("Partie terminée");
        dialog.setHeaderText("Bravo vous avez terminé la partie !");
        final File f = new File("Img/firework.gif");
        Image image = new Image(f.toURI().toString());
        ImageView iv1 = new ImageView();
        iv1.setFitWidth(image.getWidth() / 2);
        iv1.setPreserveRatio(true);
        iv1.setSmooth(true);
        iv1.setCache(true);
        iv1.setImage(image);
        dialog.setGraphic(iv1);

        ButtonType okButton = new ButtonType("Ok", ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(okButton);
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));
        dialog.getDialogPane().setContent(grid);

        grid.add(new Label("Temps: " + jeu.getTemps().getMinute() + ":" + jeu.getTemps().getSeconde()), 0, 0);
        grid.add(new Label("Nombre de coups: " + Integer.toString(jeu.getActualScore())), 0, 1);
        dialog.showAndWait();

    }

    void creerPartie() {
        Pair<String, String> pair = creationDialogue();
        jeu = new Modele_jeu();
        try {
            int nb = Integer.parseInt(pair.getKey());
            if(Integer.parseInt(pair.getKey()) > 81 || Integer.parseInt(pair.getKey()) < 1)
                nb = 40;
            jeu.createSudoku(nb, pair.getValue());
        } catch (IOException ex) {
            Logger.getLogger(Vue.class.getName()).log(Level.SEVERE, null, ex);
        }

        jeu = new Modele_jeu();
        jeu.chargerPartie(pair.getValue());

        initObserver();
    }

    void chargerPartie() {
        final FileChooser chargerFichier = new FileChooser();
        final File file = chargerFichier.showOpenDialog(null);
        if (file != null) {
            jeu = new Modele_jeu();

            jeu.chargerPartie(file.getAbsolutePath());

            initObserver();
        }
    }

    void sauvegarderPartie() {
        final FileChooser sauvegarderFichier = new FileChooser();
        final File file = sauvegarderFichier.showSaveDialog(null);
        if (file != null) {
            try {
                jeu.sauvegarderPartie(file.getAbsolutePath());
            } catch (IOException ex) {
                Logger.getLogger(Vue.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void createTutorialDialogue() {
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle("Information Dialog");
        alert.setHeaderText(null);
        alert.setResizable(true);
        alert.getDialogPane().setPrefSize(500, 350);
        String data = jeu.readFile("readme.txt");

        StringBuilder chaineFinal = jeu.splitReadme(data);



        alert.setContentText(chaineFinal.toString());


        alert.showAndWait();
    }
}
