/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc;

import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Louis Magand
 */
public class Temps extends Observable {

    public int seconde = 0, minute = 0;
    static Timer timer;
    public Thread t;

    public Temps() {
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                if (seconde > 59) {
                    seconde = 0;
                    minute++;
                }
                seconde++;
                setChanged();
                notifyObservers();
            }
        };

        t = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                }
            }
        });

        timer = new Timer("Temps");
        timer.scheduleAtFixedRate(timerTask, 1000, 1000);
        t.start();
    }

    public int getSeconde() {
        return seconde;
    }

    public int getMinute() {
        return minute;
    }

    public Timer getTimer() {
        return timer;
    }

    public Thread getThread() {
        return t;
    }

    public void setSeconde(int seconde) {
        this.seconde = seconde;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

}
