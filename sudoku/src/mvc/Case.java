/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc;
import java.util.ArrayList;

/**
 *
 * @author p1609827
 */
public abstract class Case {

    protected Valeur v;
    private ArrayList<Groupe> list_groupe;
    private Boolean conf; //Case en conflit

    public Case(Valeur v){
      this.v = v;
      list_groupe = new ArrayList<Groupe>(3);
      conf = false;
    }

    public Boolean getConf(){
      return this.conf;
    }

    public Valeur getV(){
      return this.v;
    }

    public ArrayList<Groupe> getListGroupe(){
      return this.list_groupe;
    }

    public void setConf(Boolean conf){
      this.conf = conf;
    }

    public void setListGroupe(ArrayList<Groupe> list_groupe){
      this.list_groupe = list_groupe;
    }
}
