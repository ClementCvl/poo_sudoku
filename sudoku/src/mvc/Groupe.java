/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mvc;

import java.util.ArrayList;

/**
 *
 * @author p1406664
 */
public class Groupe {

    private ArrayList<Case> list_case;

    public Groupe(){
        list_case = new ArrayList<Case>();
    }
    
    public boolean estEnConflit (Case c){
        boolean retour = false;
        for (Case c2 : list_case) {
            if( c!= c2 && c.getV() == c2.getV() ){
                retour = true;
            }
        }
        return retour;
    }

    public void addCase(Case c){
       list_case.add(c);
    }

    public void addCase(int i, Case c){
        list_case.set(i, c);
    }

    public ArrayList<Case> getList(){
        return list_case;
    }
}
