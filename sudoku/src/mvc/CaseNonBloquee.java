package mvc;

public class CaseNonBloquee extends Case{

  public CaseNonBloquee(Valeur v){
    super(v);
  }

  public void update(Valeur newV){
    this.setV(newV);
    this.setConf(Boolean.FALSE);
    for (Groupe g : this.getListGroupe()){
      if(g.estEnConflit(this) /*&& this.getListGroupe().indexOf(g) != 2*/)
        this.setConf(Boolean.TRUE);
    }
  }

  public void setV(Valeur v){
    this.v = v;
  }
}
