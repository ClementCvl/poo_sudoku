package mvc;


public enum Valeur {
  ZERO("0"),
  ONE("1"),
  TWO("2"),
  THREE("3"),
  FOUR("4"),
  FIVE("5"),
  SIX("6"),
  SEVEN("7"),
  HEIGHT("8"),
  NINE("9");


  private String v;

  private Valeur(String v){
    this.v = v;
  }
  
  public String getString(){
      return v;
  }
  
  public static Valeur getVal(String chiffre){
      Valeur val;
      
      switch (chiffre){
          case "1" :
              val = Valeur.ONE;
              break;
          case "2" :
              val = Valeur.TWO;
              break;
           case "3" :
              val = Valeur.THREE;
              break;
          case "4" :
              val = Valeur.FOUR;
              break;
          case "5" :
              val = Valeur.FIVE;
              break;
          case "6" :
              val = Valeur.SIX;
              break;
          case "7" :
              val = Valeur.SEVEN;
              break;
          case "8" :
              val = Valeur.HEIGHT;
              break;
          case "9" :
              val = Valeur.NINE;
              break;
          default :
              val = Valeur.ZERO;
              break;
      }
      return val;
  }
  
}
