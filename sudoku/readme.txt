﻿Projet effectué dans le cadre d'un projet de l'UE-INF3007L par Clement Cuvillier et Louis Magand à l'Université Lyon 1 Claude Bernard 2016/2017. 

 Nouvelle partie :.
	- Crée un sudoku à partir d'une grille remplie de 0.
	- L'algo le resout et enlève le nombre de cases choisis.
	- Vous pouvez choisir le nom du fichier.



 Sauvegarder partie :.
	- Sauvegarde l'etat et les chiffres déjà placés dans la grille.
	- Possible de nommer le fichier.



 Charger partie :.
 	- Permet de charger une partie préalablement sauvegardée.



 Resolution automatique :.
	- Résolu un sudoku dans son integralité.

 Shortcuts :.
	- Supprimer une case : DELETE ou BACKSPACE.
	- Ajouter une case : clic souris + chiffre.
	- Différents raccourcis disponibles, voir les menus.